# Package Management with Poetry

## Setting up a Python Environment

**1. Installation**

I start with a python environment that contains just some barebones libraries. This is the last thing I still use Anaconda for, and my environment is just `python 3.10`, `pip`, and `poetry`. 

This lets me easily install / update poetry. You can also install it into bash or zsh ([poetry installation](https://python-poetry.org/docs/#installation))

---
**2. Configuring Poetry**

I set one option in poetry just to keep my projects tidier and work better with Visual Studio Code: 

`poetry config virtualenvs.in-project true`

This tells poetry to create the `.venv` directory that contains the environment in the directory that you are working in. If this is false, then the environment will be stored in the default location under `poetry config cache-dir`.

## Starting/Modifying a Project

Navigate to the root directory of the project in the poetry environment and run `poetry init`. This will bring up a CLI that will request basic information about the project. The intention of poetry is to be used with packages, so if that's not what you're doing you probably don't care about most of the options. You can set the minimum python version if you know that now, or it will default to the version in your base environemnt. If you know ahead of time which dependencies you want to add, you can add them here too.

This will create a file called "pyproject.toml" that looks like something like this: 

```
[tool.poetry]
name = "{directory name}"
version = "0.1.0"
description = ""
authors = ["{user} <{email}>"]
readme = "README.md"

[tool.poetry.dependencies]
python = "^3.10"


[build-system]
requires = ["poetry-core"]
build-backend = "poetry.core.masonry.api"
```

## Poetry Shell

**1. Creating an environment**

`poetry shell` will create a new enviroment in the local directory in the `.venv` directory. Note that this environment has no libraries installed, it's just a plain python environment at the moment.

**2. Using a specific python version**

I usually have a number of different python versions installed. I install these from https://www.python.org/downloads/ using the windows installer, and these are just bare bones installs - no documentation, test suites, pip etc. I really just want a viable python.exe file.

If I want to use a different python version than exists in the base environment, I run `poetry env use {path to python.exe}` - this is just like running the `poetry shell` command, but we can use any python executable

**3. Populating the Environment**

Running `poetry shell` will put you in the poetry shell. You should see the environment name to the left of the command prompt in parenthesis. We can now run `poetry install` to install all of the dependencies we added in the pyproject.toml into this environment. 

**4. Add Dependencies**

You can add dependencies with the command `poetry add {package name}`. You can specify a version number with [operators based on semantic versioning](https://python-poetry.org/docs/dependency-specification/#using-the--operator). By default these dependencies are added under "[tool.poetry.dependencies]" in the toml file. You can also use the flag `-G (--group) {group name}` to add things like dev dependencies. When installing packages for testing or developing, we can specifically include or exclude any group.

Note that this command will also install the package, so you will probably want to be in the poetry shell when you run this command, or it will install the package into your base environment. 

Complete documentation is [here](https://python-poetry.org/docs/cli/#add).

**5. Remove Dependendencies**

`poetry remove {package name}`

**6. Update Dependendencies**

- `poetry update`: Update all dependencies
- `poetry update {package name}`: Update a specific package
- `poetry update --lock`: Don't update any packages, but update the lock file

## Running Code

You have two options to run code:

- run `poetry shell` to enter the environment. In here the command `python` will refer to the python.exe in your `.venv` folder, so you can run things like `python my_script.py`, `python -m my_module`, or `module` and they will run with the current environment.
- from your base python environment, you can run `poetry run python my_script.py`, `poetry run python -m my_module`, or `poetry run module` and it will automatically use the poetry environment. 

# Appendix

## Adding Dependencies From Private Repositories

- `poetry source add {source name} {source url}`
  - `source name` is for reference within the project, and can be anything
  - `source url` is the specific url for the package.
    - For an RWTH Gitlab project, this should look like `https://git.rwth-aachen.de/api/v4/projects/{ID}/packages/pypi/simple`
- Create a private token in gitlab
- `poetry config http-basic.{source name} {token name} {token}`
  - `source name` is the same as what was added above
  - `token name` is the name provided for this token
  - `token` is the one time use token
- `poetry add --source {source name} {package name}`
